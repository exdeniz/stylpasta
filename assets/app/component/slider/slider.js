export function slider() {
    require('slick-carousel')
    $('.sliderBig').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        cssEase: 'linear',
        asNavFor: '.sliderSmall'
    });

    $('.sliderSmall').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: false,
        dots: false,
        cssEase: 'linear',
        asNavFor: '.sliderBig',
        focusOnSelect: true
    });

    $('.sliderPhoto').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToShow: 4,
        speed: 300,
        autoplay: true,
        arrows: false,
        dots: false,
        variableWidth: true,
        centerMode: true,
        onInit: function () {
            $('.sliderPhoto').slick('slickGoTo', 8, true);
        }
    })



}
