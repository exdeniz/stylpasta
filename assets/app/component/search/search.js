export function tag() {
    $('.searchFilterItemTag .buttonDelete').click(function () {
        $(this).parent().remove()
    })
    require('selectize')
    $('.js-choiseCity').selectize({
        wrapperClass: 'selectize-control-outline',
        onChange: function (value) {
            $('.searchFilterItemTags').append('<div class="searchFilterItemTag"><button class="buttonDelete"></button>' + value + '</div>')
            $('.searchFilterItemTag .buttonDelete').click(function () {
                $(this).parent().remove()
            })
        }

    })

    $('.choiseColors div').click(function () {
        $(this).toggleClass('choiseColorActive')
    })

    $('.paginator a').click(function () {
        $(this).toggleClass('active')
    })

    $(".searchFilterItemHeader").click(function () {

        var $header = $(this);
        //getting the next element
        var $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {
            //execute this after slideToggle is done
            //change text of header based on visibility of content div
            $header.toggleClass('searchFilterItemHeaderCloapse')
        });

    });
}
