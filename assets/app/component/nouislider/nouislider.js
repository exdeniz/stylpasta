export function nouislider() {
    var noUiSlider = require('nouislider')

    var slider = document.getElementById('sliderRange')

    require('./wNumb.js')

    noUiSlider.create(slider, {
        connect: true,
        start: [0, 300000],
        step: 5000,
        range: {
            'min': [0],
            'max': [300000]
        },
        format: wNumb({
            decimals: 0,
            thousand: ' '
            // postfix: ' км.',
        })
    });

    var inputFrom = document.getElementById('inputRangeFrom');

    slider.noUiSlider.on('update', function(values, handle) {
        inputFrom.value = values[0]
    })

    inputFrom.addEventListener('change', function() {
        slider.noUiSlider.set(this.value)
    })
    var inputTo = document.getElementById('inputRangeTo');

    slider.noUiSlider.on('update', function(values, handle) {
        inputTo.value = values[1]
    })

    inputTo.addEventListener('change', function() {
        slider.noUiSlider.set([null, this.value])
    })
}


export function nouisliderPrice() {
    var noUiSlider = require('nouislider')

    var slider = document.getElementById('sliderPrice')

    require('./wNumb.js')

    noUiSlider.create(slider, {
        connect: true,
        start: [0, 10000000],
        step: 50000,
        range: {
            'min': [0],
            'max': [10000000]
        },
        format: wNumb({
            decimals: 0,
            thousand: ' '
            // postfix: ' км.',
        })
    });

    var inputFrom = document.getElementById('inputPriceFrom');

    slider.noUiSlider.on('update', function(values, handle) {
        inputFrom.value = values[0]
    })

    inputFrom.addEventListener('change', function() {
        slider.noUiSlider.set(this.value)
    })
    var inputTo = document.getElementById('inputPriceTo');

    slider.noUiSlider.on('update', function(values, handle) {
        inputTo.value = values[1]
    })

    inputTo.addEventListener('change', function() {
        slider.noUiSlider.set([null, this.value])
    })
}
