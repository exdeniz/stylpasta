export function map() {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                center: [55.751574, 37.573856],
                zoom: 16,
                controls: ['smallMapDefaultSet']
            }),
            myPlacemark = new ymaps.Placemark(myMap.getCenter(),{
                
            },  {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                iconImageHref: 'img/iconMap.png',
                iconImageSize: [ 41, 57 ], // размер иконки
                iconImageOffset: [ -41, -57 ]
            });

        myMap.geoObjects.add(myPlacemark);
    });
}
