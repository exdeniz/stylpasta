$( document ).ready(function(){
    // dealerMap.init()
    // dealerMap.getDealer(1)
    // var wallopEl = document.querySelector('.Wallop')
    // var slider = new Wallop(wallopEl)


$('select').selectize({
    wrapperClass: 'selectize-control-outline',
})

$('.selectize').selectize({
    onChange: function(value) {
        $(this)[0].$wrapper.children('.selectize-input').css({'border':'0', 'padding-top':'7px'})
        $(this)[0].$input.addClass('errorRemove')
    }
})

$('#salonChoise').selectize({
    onChange: function(value) {
        dealerMap.getDealer(parseInt(value))
        $(this)[0].$wrapper.children('.selectize-input').css({'border':'0', 'padding-top':'7px'})
        $(this)[0].$input.addClass('errorRemove')
    }
})


$('#salonChoise2').selectize({
    onChange: function(value) {
        dealerMap.getDealer(parseInt(value))
        $(this)[0].$wrapper.children('.selectize-input').css({'border':'0', 'padding-top':'7px'})
        $(this)[0].$input.addClass('errorRemove')
    }
})


$('input').iCheck()

$('input').on('ifChecked', function(event){
    $('.icheckbox').removeClass('error')
})


$('.Switcher').on('ifChecked',function (e) {
    $(this).parent().parent().addClass('contentSwitcherItemActive')
    slider.next()
})

$('.Switcher').on('ifUnchecked',function (e) {
    $(this).parent().parent().removeClass('contentSwitcherItemActive')
})

$('#datepicker').datepicker({
    language: 'ru',
    format: 'dd-mm-yyyy',
    datesDisabled: ['10-11-2015', '01-11-2015']
    }).on("changeDate", function(e) {
        $('#datepickerInput').val(
            $('#datepicker').datepicker('getFormattedDate')
        )
    })

$('.disabled').qtip({ // Grab some elements to apply the tooltip to
    content: {
        text: 'Дата забронирована'
    },
    position: {
        my: 'bottom center',  // Position my top left...
        at: 'top center', // at the bottom right of...

    }
})

$('.timepicker').timepicker({
    disableTimeRanges: [['0:00am', '8:00am'], ['5:00pm', '8:00pm']],
    'timeFormat': 'H:i',
    step: 60,
    'minTime': '8:00am',
    'maxTime': '23:00pm',
});
$('.inputMaskPhone').inputmask("+7 (999) 999-9999",{
    showMaskOnHover: false
})
$('.inputMaskDate').inputmask("dd-mm-yyyy",
    {
        "placeholder": "дд-мм-гггг",
        showMaskOnHover: false,
        "oncomplete": function(e){
            $('#datepicker').datepicker("setDate", $('.inputMaskDate').val())
        }
})
jQuery.extend(jQuery.validator.messages, {
    required: "Это поле должно быть заполнено",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

$('#formMain').validate({
    ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
    rules: {
        email: {
            required: true,
            email: true
        },
        accept: {
            required: true
        }
    },
    messages: {
        required: "Это поле должно быть заполнено",
        Lastname: {
            required: "Пожалуйста введите вашу фамилию"
        },
        Firstname: {
            required: "Пожалуйста введите ваше имя"
        },
        phone: {
            required: "Пожалуйста введите ваш телефон"
        },
        email: {
            required: "Нужно заполнить e-mail",
            email: "Введите правильный e-mail"
        },
        date: {
            required: ""
        }
    }
})

$(".jsMainValid").click(function () {
    $('#formMain').valid()
    if (!$('.icheckbox').hasClass('checked')) {
        $('.icheckbox').addClass('error')
    }
})

$('#formOther').validate({
    ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input, .icheckbox input',
    rules: {
        email: {
            required: true,
            email: true
        },
        accept: {
            required: true
        }
    },
    messages: {
        required: "Это поле должно быть заполнено",
        Lastname: {
            required: "Пожалуйста введите вашу фамилию"
        },
        Firstname: {
            required: "Пожалуйста введите ваше имя"
        },
        phone: {
            required: "Пожалуйста введите ваш телефон"
        },
        email: {
            required: "Нужно заполнить e-mail",
            email: "Введите правильный e-mail"
        },
        date: {
            required: ""
        }
    }
})
$(".jsOtherValid").click(function () {
    $('#formOther').valid()
    if (!$('.icheckbox').hasClass('checked')) {
        $('.icheckbox').addClass('error')
    }
})

    $('.jsModalClose').click(function () {
        $('.modal').removeClass('modalOpen')
        $('.modalOverlay').removeClass('modalOverlayOpen')
    })

    $('.logo').click(function () {
        $('.modal').addClass('modalOpen')
        $('.modalOverlay').addClass('modalOverlayOpen')
    })
    $('.sm-js').smartmenus({
        subIndicators: false,
        subMenusMinWidth: '100%',
        mainMenuSubOffsetX: 1
    })
})
