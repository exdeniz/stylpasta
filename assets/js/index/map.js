var dealerMap = (function () {
var mapDealersMain, geoObjectsCluster

var placemarkList = [
  {
    "Id":101,
    "Name":"ПАНАВТО",
    "Address":"50-й км МКАД, внешняя сторона, между Можайским шоссе и Мичуринским проспектом",
    "Phone":"(495) 790-7777",
    "Fax":"(495) 790-7777",
    "Site":"http://www.mercedes-panavto.ru",
    "Latitude":55.685122,
    "Longitude":37.413538
  },{
    "Id":106,
    "Name":"ПМ-Авто",
    "Address":"ул. Павловский тракт, 249в",
    "Phone":"(3852) 467-467",
    "Fax":"(3852) 462-496",
    "Site":"http://www.mercedes-pm-auto.ru",
    "Latitude":53.342033,
    "Longitude":83.690224
  },{
    "Id":9,
    "Name":"Авангард",
    "Address":"Приморский проспект., 54, корп.4",
    "Phone":"(812) 333-33-44",
    "Fax":"(812) 333-38-16",
    "Site":"http://www.mercedes-avangard.ru",
    "Latitude":59.984002,
    "Longitude":30.231743
  },{
    "Id":108,
    "Name":"Агат-МБ",
    "Address":"пр. Ленина, 122",
    "Phone":"(8442) 55 16 55",
    "Fax":"(8442) 55 16 55",
    "Site":"http://www.mercedes-agat.ru",
    "Latitude":48.788139,
    "Longitude":44.593532
  },{
    "Id":109,
    "Name":"АврораАвто",
    "Address":"ул. Дорожная 12",
    "Phone":"(4732) 78-85-85",
    "Fax":"(4732) 72-08-02",
    "Site":"http://www.mercedes-avroraavto.ru",
    "Latitude":51.664078,
    "Longitude":39.151085
  },{
    "Id":2,
    "Name":"Авилон (Волгоградский пр-т)",
    "Address":"Волгоградский пр-т, д.43, корп. 2",
    "Phone":"(495) 730-44-44",
    "Fax":"(495) 730-44-49",
    "Site":"http://www.mercedes-avilon.ru",
    "Latitude":55.713781,
    "Longitude":37.719649
  },{
    "Id":3,
    "Name":"Автофорум",
    "Address":"МКАД 92 км",
    "Phone":"(495) 730-11-11",
    "Fax":"(495) 730-11-35",
    "Site":"http://www.mercedes-autoforum.ru",
    "Latitude":55.894556,
    "Longitude":37.70008
  },{
    "Id":11,
    "Name":"Автофорум Нева",
    "Address":"ул. Орбели, 35",
    "Phone":"(812) 329-1000",
    "Fax":"(812) 329-1001",
    "Site":"http://www.mercedes-autoforum-spb.ru",
    "Latitude":60.007459,
    "Longitude":30.340851
  },{
    "Id":16,
    "Name":"Адонис-Авто",
    "Address":"ул. Беляева, 35 (Тобольский тракт)",
    "Phone":"+7 (3452) 500-528",
    "Fax":"+7 (3452) 500-528",
    "Site":"http://www.mercedes-adonis-auto.ru",
    "Latitude":57.174633,
    "Longitude":65.652237
  },{
    "Id":17,
    "Name":"Альтаир-Авто",
    "Address":"Приморский край, г. Артём, ул. Тульская, 20",
    "Phone":"(423) 2-383-383",
    "Fax":"(423) 2-383-383",
    "Site":"http://www.mercedes-altair-auto.ru",
    "Latitude":43.314262,
    "Longitude":132.090164
  },{
    "Id":18,
    "Name":"Арт-Моторс МБ",
    "Address":"проспект Салавата Юлаева, 60",
    "Phone":"+7 (347) 241-50-50",
    "Fax":"+7 (347) 241-50-50",
    "Site":"http://www.mercedes-art-motors.ru",
    "Latitude":54.751892,
    "Longitude":56.020565
  },{
    "Id":19,
    "Name":"Атлас",
    "Address":"Лежневское шоссе, стр. 5",
    "Phone":"(4932) 56-29-56",
    "Fax":"(4932) 56-29-56",
    "Site":"http://www.mercedes-ivanovo.ru",
    "Latitude":56.94886,
    "Longitude":40.974712
  },{
    "Id":20,
    "Name":"Влако-сервис",
    "Address":"Хрящевское ш., 11 ",
    "Phone":"(8482) 69 80 80",
    "Fax":"(8482) 69 80 80",
    "Site":"http://www.mercedes-wlako.ru/",
    "Latitude":53.56749,
    "Longitude":49.34927
  },{
    "Id":22,
    "Name":"УралАвтоХаус Екатеринбург",
    "Address":"620043, Московский тракт 8-й км, строение 27",
    "Phone":"(343) 311-11-00",
    "Fax":"(343) 311-11-05",
    "Site":"http://www.mercedes-ekb.ru",
    "Latitude":56.819944,
    "Longitude":60.506886
  },{
    "Id":23,
    "Name":"Евролак",
    "Address":"Московский пр., 250",
    "Phone":"(4012) 728-207",
    "Fax":"(4012) 728-201",
    "Site":"http://www.mercedes-kaliningrad.ru",
    "Latitude":54.707803,
    "Longitude":20.574365
  },{
    "Id":12,
    "Name":"Звезда Невы (Витебский проспект)",
    "Address":"Витебский пр.,17, к.1",
    "Phone":"(812) 331-33-01",
    "Fax":"(812) 388-97-70",
    "Site":"http://www.mercedes-zvezdanevy.ru",
    "Latitude":59.871666,
    "Longitude":30.353333
  },{
    "Id":4,
    "Name":"Звезда Столицы Варшавка (филиал Рольф)",
    "Address":"Варшавское шоссе, 127",
    "Phone":"(495) 974 22 22",
    "Fax":"(495) 974 62 32",
    "Site":"http://www.mercedes-zs.ru",
    "Latitude":55.620852,
    "Longitude":37.616181
  },{
    "Id":27,
    "Name":"Икар",
    "Address":"ул. Аэропорт, д. 20",
    "Phone":"(8452) 55-66-77",
    "Fax":"(8452) 55-66-77 ",
    "Site":"http://www.mercedes-icar.ru",
    "Latitude":51.556024,
    "Longitude":46.040037
  },{
    "Id":28,
    "Name":"Каскад-Авто",
    "Address":"Оренбургская область, Оренбургский район, п. Пригородный, ул.13 км. Шоссе Оренбург-Орск",
    "Phone":"(3532) 91-01-02",
    "Fax":"(3532) 91-01-02",
    "Site":"http://www.mercedes-orenburg.ru",
    "Latitude":51.771145,
    "Longitude":55.267012
  },{
    "Id":29,
    "Name":"СБСВ-Ключавто Север",
    "Address":"ул. Покрышкина, 15/1",
    "Phone":"(861) 260-00-00, 222-22-77",
    "Fax":"(861) 260-00-00, 222-22-77",
    "Site":"http://www.mercedes-krasnodar.ru",
    "Latitude":45.104848,
    "Longitude":38.970064
  },{
    "Id":6,
    "Name":"ЛУКОЙЛ-Центрнефтепродукт",
    "Address":"Новорижское ш., 7 км от МКАД",
    "Phone":"(495) 771-7232",
    "Fax":"(495) 771-7231",
    "Site":"http://www.mercedes-lukoil.ru",
    "Latitude":55.788395,
    "Longitude":37.252811
  },{
    "Id":31,
    "Name":"Автокласс",
    "Address":"ул. Студенческая 1-Т, стр.2 ",
    "Phone":"(4722) 376-101 ",
    "Fax":"(4722) 376-101",
    "Site":"http://www.mercedes-belgorod.ru",
    "Latitude":50.617567,
    "Longitude":36.61039
  },{
    "Id":7,
    "Name":"МБ-Беляево",
    "Address":"ул. Академика Волгина, 6А",
    "Phone":"(495) 788-1010",
    "Fax":"(495) 336-1909",
    "Site":"http://www.mercedes-mb-belyaevo.ru",
    "Latitude":55.652718,
    "Longitude":37.51506
  },{
    "Id":8,
    "Name":"МБ-Измайлово",
    "Address":"МО, Горьковское шоссе, 1 км от МКАД",
    "Phone":"(495) 660-00-00",
    "Fax":"(495) 660-00-00",
    "Site":"http://www.mercedes-izmaylovo.ru",
    "Latitude":55.78504,
    "Longitude":37.871387
  },{
    "Id":34,
    "Name":"Мерседес-Бенц РУС (Вешки)",
    "Address":"МО, Мытищинский район, 85 км МКАД, пересечение с Алтуфьевским шоссе. ТПЗ «Алтуфьево», Автомобильный проезд, вл. 5, стр. 1-9",
    "Phone":"(495) 225-0000",
    "Fax":"(495) 663-66-01 ",
    "Site":"http://www.mercedes-mbr.ru",
    "Latitude":55.91547,
    "Longitude":37.583569
  },{
    "Id":35,
    "Name":"Мерседес-Бенц РУС (Ленинградский проспект)",
    "Address":"Ленинградский пр-т, 39а",
    "Phone":"(495) 225-0000",
    "Fax":"(495) 225-0004",
    "Site":"http://www.mercedes-mbr.ru",
    "Latitude":55.795396,
    "Longitude":37.541433
  },{
    "Id":36,
    "Name":"МЦ-Иркутск",
    "Address":"ул. Ширямова 32",
    "Phone":"(3952) 500-711",
    "Fax":"-",
    "Site":"http://www.mercedes-irkutsk.ru",
    "Latitude":52.263889,
    "Longitude":104.343887
  },{
    "Id":37,
    "Name":"Новотех-МБ",
    "Address":"Югорский тракт, 36",
    "Phone":"+7 (3462) 77-44-33",
    "Fax":"+7 (3462) 77-44-33",
    "Site":"http://www.mercedes-novotekh.ru",
    "Latitude":61.242054,
    "Longitude":73.369316
  },{
    "Id":39,
    "Name":"Омега",
    "Address":"ул.Игуменка, 181",
    "Phone":"(351) 211-44-77",
    "Fax":"(351) 211-08-18",
    "Site":"http://www.mercedes-omega.ru",
    "Latitude":55.082513,
    "Longitude":61.390483
  },{
    "Id":40,
    "Name":"Орион",
    "Address":"ул. 9 Мая 2г",
    "Phone":"(391) 274-95-00",
    "Fax":"(391) 274-95-25",
    "Site":"http://www.mercedes-orion.ru",
    "Latitude":56.069843,
    "Longitude":92.93486
  },{
    "Id":41,
    "Name":"СБСВ-КЛЮЧАВТО ДОН (Вятская)",
    "Address":"ул. Вятская, 116/3 ",
    "Phone":"(863) 231-77-77",
    "Fax":"(863) 231-77-77",
    "Site":"http://www.mercedes-rnd.ru",
    "Latitude":47.2799,
    "Longitude":39.76614
  },{
    "Id":42,
    "Name":"СБСВ-КЛЮЧАВТО ДОН (Пойменная)",
    "Address":"ул. Пойменная, 1г",
    "Phone":"(863) 231-77-77",
    "Fax":"(863) 231-77-77",
    "Site":"http://www.mercedes-rnd.ru",
    "Latitude":47.205107,
    "Longitude":39.727248
  },{
    "Id":43,
    "Name":"Плаза",
    "Address":"603137, проспект Гагарина, д.230",
    "Phone":"(831) 462-7000",
    "Fax":"(831) 462-7000",
    "Site":"http://www.mercedes-nn.ru",
    "Latitude":56.225196,
    "Longitude":43.942091
  },{
    "Id":44,
    "Name":"Самара-Моторс",
    "Address":"Московское Шоссе, 17 км, д. 15.",
    "Phone":"(846) 342 55 55",
    "Fax":"(846) 342 55 55",
    "Site":"http://www.mercedes-samara-motors.ru",
    "Latitude":53.273834,
    "Longitude":50.262451
  },{
    "Id":45,
    "Name":"Сот моторс",
    "Address":"пр. К. Маркса, 18 ",
    "Phone":"+7 (3812) 32-09-92",
    "Fax":"+7 (3812) 32-09-92",
    "Site":"http://www.mercedes-sotmotors.ru",
    "Latitude":54.9659,
    "Longitude":73.384117
  },{
    "Id":46,
    "Name":"СТС-автомобили (Кемерово)",
    "Address":"трасса Р-384 Кемерово-Новокузнецк, 4й км",
    "Phone":"+7-3842-77-55-55",
    "Fax":"+7-3842-77-55-55",
    "Site":"http://www.mercedes-sts.ru",
    "Latitude":55.262179,
    "Longitude":86.164352
  },{
    "Id":47,
    "Name":"СТС-автомобили (Новосибирск)",
    "Address":"ул. Большевистская, д.1",
    "Phone":"(383) 363-98-70",
    "Fax":"(383) 363-98-70",
    "Site":"http://www.mercedes-sts.ru",
    "Latitude":55.012992,
    "Longitude":82.92749
  },{
    "Id":48,
    "Name":"СТС-автомобили (Томск)",
    "Address":"ул. Пушкина, 61А",
    "Phone":"(3822) 90 00 64",
    "Fax":"(3822) 90 00 64",
    "Site":"http://www.mercedes-sts.ru",
    "Latitude":56.500107,
    "Longitude":84.98838
  },{
    "Id":49,
    "Name":"Телта-МБ",
    "Address":"ул. Крисанова, д. 4",
    "Phone":"(342) 239-20-39, 239-20-65",
    "Fax":"(342) 236-1943",
    "Site":"http://www.mercedes-perm.ru",
    "Latitude":58.0123,
    "Longitude":56.2103
  },{
    "Id":51,
    "Name":"УралАвтоХаус",
    "Address":"г. Копейск, пр. Победы, 71.",
    "Phone":"(351) 2 555 666",
    "Fax":"(351) 2 555 666",
    "Site":"http://www.mercedes-uah.ru",
    "Latitude":55.118622,
    "Longitude":61.54481
  },{
    "Id":52,
    "Name":"УралАвтоХаус Магнитогорск",
    "Address":"пр.Ленина, 97-Б",
    "Phone":"(3519) 43-8888",
    "Fax":"-",
    "Site":"http://www.mercedes-magnitogorsk.ru",
    "Latitude":53.38617444,
    "Longitude":58.9916707
  },{
    "Id":53,
    "Name":"Центр-Кама",
    "Address":"пр. Набережночелнинский, 19А ",
    "Phone":"(8552) 444-777",
    "Fax":"(8552) 33-06-31",
    "Site":"http://www.mercedes-kama.ru",
    "Latitude":55.707043,
    "Longitude":52.354538
  },{
    "Id":54,
    "Name":"Штерн",
    "Address":"ул. Челюскинцев, 10",
    "Phone":"(343) 377 62 10",
    "Fax":"(343) 377 62 11",
    "Site":"http://www.mercedes-stern.ru",
    "Latitude":56.843025,
    "Longitude":60.584174
  },{
    "Id":57,
    "Name":"СБСВ КЛЮЧАВТО Горячий ключ",
    "Address":"дорога Москва-Новороссийск, 1384км.+470м слева",
    "Phone":"(86159) 44-405      ",
    "Fax":"(86159) 44-405",
    "Site":"http://www.mercedes-gk.ru",
    "Latitude":44.650366,
    "Longitude":39.152467
  },{
    "Id":58,
    "Name":"СБСВ-Ключавто Ставрополь",
    "Address":"пр-т Кулакова, 3",
    "Phone":"(8652) 56-20-20      ",
    "Fax":"(8652) 56-20-20",
    "Site":"http://www.mercedes-stavropol.ru",
    "Latitude":45.041592,
    "Longitude":41.912495
  },{
    "Id":63,
    "Name":"КЛЮЧАВТО-КМВ",
    "Address":"Ставропольский край, Минераловодский район, автодорога «Кавказ» 345 км",
    "Phone":"(87922) 97777",
    "Fax":"(87922) 97777",
    "Site":"http://www.mercedes-kmv.ru",
    "Latitude":44.223,
    "Longitude":43.059198
  },{
    "Id":65,
    "Name":"Скандинавский диалог",
    "Address":"Матвеевское шоссе, 40",
    "Phone":"(4212) 26-20-69",
    "Fax":"-",
    "Site":"http://www.mercedes-scad.ru",
    "Latitude":48.530762,
    "Longitude":135.176132
  },{
    "Id":66,
    "Name":"Вега-Авто",
    "Address":"ул. Марголина, д. 19",
    "Phone":"(4852) 42-60-60",
    "Fax":"(4852) 42-60-60",
    "Site":"http://www.mercedes-yaroslavl.ru",
    "Latitude":57.555653,
    "Longitude":39.944626
  },{
    "Id":68,
    "Name":"МБ-Липецк",
    "Address":"Воронежское шоссе, 2А ",
    "Phone":"(4742) 51-52-52 ",
    "Fax":"-",
    "Site":"http://www.mercedes-lipetsk.ru",
    "Latitude":52.61446,
    "Longitude":39.528507
  },{
    "Id":69,
    "Name":"МБ-Тверь",
    "Address":"ул. Малые Перемерки, д.42",
    "Phone":"(4822) 65-69-69",
    "Fax":"-",
    "Site":"http://www.mercedes-tver.ru",
    "Latitude":56.810081,
    "Longitude":36.001877
  },{
    "Id":13,
    "Name":"Звезда Невы (Крестьянский переулок)",
    "Address":"Крестьянский переулок., д.4а ",
    "Phone":"(812) 331 33 01",
    "Fax":"(812) 331 33 08",
    "Site":"http://www.mercedes-zvezdanevy.ru",
    "Latitude":59.956656,
    "Longitude":30.321644
  },{
    "Id":73,
    "Name":"Р-Моторс",
    "Address":"ул.Видова 147",
    "Phone":"(8617) 77-70-77",
    "Fax":"(8617) 77-70-07",
    "Site":"http://www.mercedes-rmotors.ru",
    "Latitude":44.737307,
    "Longitude":37.731481
  },{
    "Id":10,
    "Name":"Авто-Пулково",
    "Address":"Пулковское шоссе, д.14, литер  А",
    "Phone":"(812) 410-1000",
    "Fax":"(812) 410-1010",
    "Site":"http://www.mercedes-pulkovo.ru",
    "Latitude":59.83066,
    "Longitude":30.32763
  },{
    "Id":75,
    "Name":"МБ-Киров",
    "Address":"ул. Дзержинского, 77",
    "Phone":"+7 (8332) 24-77-77",
    "Fax":"-",
    "Site":"http://www.mercedes-kirov.ru",
    "Latitude":58.636895,
    "Longitude":49.593175
  },{
    "Id":5,
    "Name":"Звезда Столицы Каширка (филиал Рольф)",
    "Address":"Московская область, Ленинский район, п. Развилка",
    "Phone":"+7 (495) 974-22-23",
    "Fax":"-",
    "Site":"http://www.mercedes-zsk.ru",
    "Latitude":55.59179102,
    "Longitude":37.73334985
  },{
    "Id":77,
    "Name":"Кардинал",
    "Address":"ул. Ликбеза, 3а",
    "Phone":"(4872) 70-35-35",
    "Fax":"(4872) 70-35-35",
    "Site":"http://www.mercedes-cardinal.ru",
    "Latitude":54.208579,
    "Longitude":37.571807
  },{
    "Id":78,
    "Name":"МБ-Ирбис (улица Космонавтов)",
    "Address":"ул. Космонавтов, д.71",
    "Phone":"(843) 294-94-94",
    "Fax":"(843) 294-94-96",
    "Site":"http://www.mercedes-irbis.ru",
    "Latitude":55.796643,
    "Longitude":49.210021
  },{
    "Id":80,
    "Name":"АБС-АВТО Сочи",
    "Address":"ул. Ленина, 286Ж",
    "Phone":"(8622) 46-36-36",
    "Fax":"-",
    "Site":"http://www.mercedes-sochi.ru",
    "Latitude":43.474531,
    "Longitude":39.899114
  },{
    "Id":81,
    "Name":"МБ-Двина",
    "Address":"ул. Октябрят, 31",
    "Phone":"(8182) 42 25 25 ",
    "Fax":"(8182) 42 25 35 ",
    "Site":"http://www.mercedes-dvina.ru",
    "Latitude":64.536586,
    "Longitude":40.59962
  },{
    "Id":83,
    "Name":"МБ-Владимир",
    "Address":"ул. Куйбышева, д.28",
    "Phone":"(4922) 47-47-47",
    "Fax":"-",
    "Site":"http://www.mercedes-vladimir.ru",
    "Latitude":56.170781,
    "Longitude":40.414297
  },{
    "Id":84,
    "Name":"АСМОТО МБ",
    "Address":"ул. Билимбаевская, 8б",
    "Phone":"(343) 311-17-71",
    "Fax":"-",
    "Site":"http://www.mercedes-asmoto.ru",
    "Latitude":56.881426,
    "Longitude":60.521706
  },{
    "Id":85,
    "Name":"МОТОМ ПРЕМИУМ",
    "Address":"Московское шоссе, 100. 1",
    "Phone":"(8422) 58-07-07",
    "Fax":"-",
    "Site":"http://www.mercedes-motom.ru/",
    "Latitude":54.305521,
    "Longitude":48.355177
  },{
    "Id":1,
    "Name":"Авилон (Воздвиженка)",
    "Address":"ул. Воздвиженка, д.12",
    "Phone":"(495) 781-77-07",
    "Fax":"(495) 781-77-07",
    "Site":"http://www.mercedes-vozdvizhenka.ru",
    "Latitude":55.752957,
    "Longitude":37.605113
  },{
    "Id":90,
    "Name":"СТС-автомобили (Обь)",
    "Address":"г. Обь, Омский тракт, 1/2",
    "Phone":"+7 (383) 363 95 62",
    "Fax":"+7 (383) 363 95 62",
    "Site":"http://www.mercedes-sts.ru",
    "Latitude":54.99851338,
    "Longitude":82.67794452
  },{
    "Id":91,
    "Name":"СБСВ-КЛЮЧАВТО Премиум",
    "Address":"ул. Крылатая, д. 12",
    "Phone":"+7 861 222 22 77",
    "Fax":"+7 861 222 22 77",
    "Site":"http://www.mercedes-krasnodar.ru/",
    "Latitude":45.020802,
    "Longitude":39.125821
  },{
    "Id":92,
    "Name":"МБ-Савино",
    "Address":"ул. Шоссе Космонавтов 413 а",
    "Phone":"+7 (342) 259-44-44",
    "Fax":"-",
    "Site":"http://www.mercedes-savino.ru/",
    "Latitude":57.9452,
    "Longitude":56.0791
  },{
    "Id":96,
    "Name":"Автомир Премиум",
    "Address":"ул. Воронежская, 85/5",
    "Phone":"(4212) 70-70-70",
    "Fax":"(4212) 70-70-70",
    "Site":"http://www.mercedes-khabarovsk.ru",
    "Latitude":48.51946069,
    "Longitude":135.0713198
  },{
    "Id":98,
    "Name":"МБ-Чебоксары",
    "Address":"428000, проспект Тракторостроителей, д. 123",
    "Phone":"+7 (8352) 202-204",
    "Fax":"+7 (8352) 202-204",
    "Site":"http://www.mercedes-cheboksary.ru",
    "Latitude":56.120091,
    "Longitude":47.359356
  },{
    "Id":104,
    "Name":"МБ-Орловка",
    "Address":"Мензелинский тракт, 24",
    "Phone":"+7 (8552) 927-927",
    "Fax":"-",
    "Site":"http://www.mercedes-orlovka.ru",
    "Latitude":55.704123,
    "Longitude":52.400635
  },{
    "Id":108,
    "Name":"МБ-Ирбис (проспект Ямашева)",
    "Address":"Проспект Ямашева, д.122Б",
    "Phone":"(843) 294-94-94",
    "Fax":"(843) 294-94-80",
    "Site":"http://www.mercedes-irbis.ru",
    "Latitude":55.8251,
    "Longitude":49.1632
  }];



    var collection,
        balloonsList = [],
        map = document.querySelector('#map'),
        map2 = document.querySelector('#map2'),
        yaMap = {},
        yaMap2 = {}

    var mapInterface = {

        initMap: function (coords, zoom) {
            yaMap = new ymaps.Map('map', {
                center: [55.76, 37.64],
                control: 'none',
                zoom: 16
            })
            yaMap2 = new ymaps.Map('map2', {
                center: [55.76, 37.64],
                control: 'none',
                zoom: 16
            })
            yaMap.geoObjects.events.add([
                'balloonopen'
            ], function (e) {
                var geoObject = e.get('target')
                yaMap.panTo(geoObject.geometry.getCoordinates(), {
                    delay: 0,
                    duration: 300
                })
            })
            yaMap2.geoObjects.events.add([
                'balloonopen'
            ], function (e) {
                var geoObject = e.get('target')
                yaMap.panTo(geoObject.geometry.getCoordinates(), {
                    delay: 0,
                    duration: 300
                })
            })
           },
        addBallon: function (id) {
            salonId = id
            ymaps.ready(function() {
                var arrayId = _.findIndex(placemarkList,{
                    Id: salonId
                })
                // console.log(placemarkList[arrayId])
                placemarkItem = placemarkList[arrayId]
                var balloonContent = '<div class="balloonContent">'
                balloonContent += '<div class="balloonContentTitle">'+ placemarkItem.Name +'</div>';
                balloonContent += '<div class="balloonContentAddress">'+ placemarkItem.Address +'</div>';
                balloonContent += '<div class="balloonContentPhone">'+ placemarkItem.Phone +'</div>';
                balloonContent += '<div class="balloonContentFax">'+ placemarkItem.Fax +'</div>';
                balloonContent += '<div class="balloonContentSite"><a href="'+ placemarkItem.Site +'" target="_blank">'+ placemarkItem.Site +'</a></div>';
                var Latitude = placemarkList[arrayId].Latitude
                var Longitude = placemarkList[arrayId].Longitude
                var placemarkOptions = {
                    iconImageHref: 'img/ico-map-active.png', // картинка иконки
                    iconImageSize: [40, 56],
                    iconImageOffset: [-15, -56],
                    hideIconOnBalloonOpen: false,
                    //balloonContentSize: [270, 99],
                    balloonShadow: false,
                    balloonOffset: [0, -175],
                    balloonLayout: "default#imageWithContent"
                    //balloonShadowPane: 'movableOuters'

                }
                var myPlacemark = new ymaps.Placemark([Latitude, Longitude],{
                        balloonContent: balloonContent
                    }, placemarkOptions)
                var myPlacemark2 = new ymaps.Placemark([Latitude, Longitude],{
                        balloonContent: balloonContent
                    }, placemarkOptions)
                yaMap2.geoObjects.add(myPlacemark2)
                yaMap2.setCenter([Latitude, Longitude])
                yaMap.geoObjects.add(myPlacemark)
                yaMap.setCenter([Latitude, Longitude])

                myPlacemark.balloon.open()
                myPlacemark2.balloon.open()
            })
        },
        loadBalloons: function () {
            var placemarkOptions = {
                iconImageHref: 'img/ico-map-active.png', // картинка иконки
                iconImageSize: [ 80, 103 ], // размер иконки
                iconImageOffset: [ -39, -100 ], // позиция иконки
                balloonContentSize: [ 470, 391 ], // размер нашего кастомного балуна в пикселях
                balloonLayout: 'default#imageWithContent', // указываем что содержимое балуна кастомная херь
                //balloonImageHref: 'img/baloon_bg.png', // Картинка заднего фона балуна
                balloonImageOffset: [ -475, -160 ], // смещание балуна, надо подогнать под стрелочку
                //balloonImageSize: [249, 103], // размер картинки-бэкграунда балуна
                balloonShadow: false,
                balloonAutoPan: false,
                hideIconOnBalloonOpen: false,
                balloonCloseButton: false
                //visible: false
            }
        }

    }
    function setSiteFullWindow() {
        if (document.querySelector('#contacts-map')) {
            var contactMap = document.querySelector('.contactMap')
            var wH = window.innerHeight
            var headerH = document.querySelector('.header').offsetHeight
            var filterH = document.querySelector('.contactFilter').offsetHeight
            var footerH = document.querySelector('.footer').offsetHeight
            var mapH = wH - headerH - filterH - footerH - 30
            contactMap.style.height = mapH + 'px'
        }
    }
    return {
        init: function () {

            ymaps.ready(mapInterface.initMap)
        },
        resize: function () {
            setSiteFullWindow()
            contactsMap.container.fitToViewport()
        },
        getDealer: function (value) {
            mapInterface.addBallon(value)
        }
    }
}());
