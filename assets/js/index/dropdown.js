var dropdowns = document.querySelectorAll('.dropdown');

for (var i = 0, len = dropdowns.length; i < len; i++) {
    var buttonDropdown = dropdowns[i].getElementsByClassName('dropdownButton')
    var listDropdown = dropdowns[i].getElementsByClassName('dropdownList')
    buttonDropdown[0].addEventListener('click', function(e) {
        if (listDropdown[0].style.opacity == 0) {
            listDropdown[0].style.opacity = 1
            listDropdown[0].style.visibility = 'visible'
            this.classList.add('dropdownButtonActive')
        } else {
            listDropdown[0].style.opacity = 0
            listDropdown[0].style.visibility = 'hidden'
            this.classList.remove('dropdownButtonActive')
        }
    }, false)
}


// var dropdownsBlack = document.querySelectorAll('.dropdownBlack');
//
// for (var i = 0, len = dropdownsBlack.length; i < len; i++) {
//     var buttonDropdown = dropdownsBlack[i].getElementsByClassName('dropdownBlackButtonTrigger')
//     var listDropdown = dropdownsBlack[i].getElementsByClassName('dropdownBlackList')
//     buttonDropdown[0].addEventListener('click', function(e) {
//         if (listDropdown[0].style.opacity == 0) {
//             listDropdown[0].style.opacity = 1
//             this.parentNode.classList.add('dropdownBlackButtonActive')
//         } else {
//             listDropdown[0].style.opacity = 0
//             this.parentNode.classList.remove('dropdownBlackButtonActive')
//         }
//     }, false)
// }
