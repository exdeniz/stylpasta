    var hapi = require('hapi');

    var server = new hapi.Server();

    // add connection parameters
    server.connection({
        port: 3010
        host: 'localhost',
    });

    server.views({
        engines: {
            jade: {
                module: require('jade'),
                isCached: false
            }
        },
        path: 'assets/template'
            // layoutPath: 'assets/template',
            // layout: 'default',
            // //helpersPath: 'views/helpers',
            //partialsPath: 'views/partials'
    });

    // create your routes, currently it's just one
    var routes = [{
        method: 'GET',
        path: '/favicon.ico',
        handler: {
            file: './favicon.ico'
        }

    }, {
        method: 'GET',
        path: '/',
        handler: function(request, reply) {
            // Render the view with the custom greeting
            var data = {
                title: 'This is Index!',
                message: 'Hello, World. You crazy handlebars layout'
            };

            return reply.view('index', data);
        }
    }, {
        method: 'GET',
        path: '/{filename}',
        handler: function(request, reply) {
            // Render the view with the custom greeting
            var data = {
                title: 'This is Index!',
                message: 'Hello, World. You crazy handlebars layout'
            };

            return reply.view(request.params.filename, data);
        }
    }, {
        path: "/{path*}",
        method: "GET",
        handler: {
            directory: {
                path: "./public",
                listing: false,
                index: false
            }
        }
    }];

    // tell your server about the defined routes
    server.route(routes);

    // Start the server
    server.register([

        {
            register: require("hapi-cache-buster")
        }
    ], function() {
        //Start the server
        server.start(function() {
            //Log to the console the host and port info
            console.log('Server started at: ' + server.info.uri);
        });


    })
